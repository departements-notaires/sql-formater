<?php

$srcDir = './data-src';
$srcFile = 'donnees_test_instructeurs_territoire.sql';
$srcPath = "$srcDir/$srcFile";
$outputPath = "./data-output/$srcFile";



mb_internal_encoding('utf-8'); // @important
$srcLines = file($srcPath);
$srcLinesCount = count($srcLines);
$outputLines = [];
$selectedLines = []; // ligne SQL à traiter
$badLines = [];     // ligne SQL à traiter, mais qui pose problème dans le découpage
$colInfo = []; // information sur les colonnes des lignes à traiter
$maxCol = 15;
$commentLineCount = 0;
$genericSqlLineCount = 0;
foreach ($srcLines as $srcLineNumber => $srcLine) {
    if($srcLineNumber < 3 && $srcLineNumber !== 1) { // 1er lignes SQL
        $outputLines[$srcLineNumber] = trim($srcLine);
        unset($srcLines[$srcLineNumber]);
        $genericSqlLineCount++;
    }
    else if( substr("$srcLine", 0, 5) === '-- --' ) { // lignes de commentaires
        $outputLines[$srcLineNumber] = trim($srcLine);
        unset($srcLines[$srcLineNumber]);
        $commentLineCount++;
    }
    else if( substr("$srcLine", 0, 1) !== '(' ) { // autres lignes non valide
        throw new Exception("\nERROR : bad line n°$srcLineNumber\n[$srcLine]\n");
    }
    else {
        $cleanSrcLine = trim("$srcLine");
        $cleanSrcLine = trim("$cleanSrcLine", "(");
        $cleanSrcLine = trim("$cleanSrcLine", ",");
        $cleanSrcLine = trim("$cleanSrcLine", ")");
        $dataLine = explode(", ", "$cleanSrcLine");
        $dataLineCount = count($dataLine);
        if($dataLineCount > ($maxCol) ) {
            $errorMsg = "\nERROR: mauvais décopuage de la ligne \n---> trop de colonnes : $dataLineCount vs $maxCol colonnes attendues\n";
            echo "-------------------\n$errorMsg\n$cleanSrcLine\n"; print_r($dataLine); echo "\n\n";
            $badLines[$srcLineNumber]['srcLine'] = trim($srcLine);
            $badLines[$srcLineNumber]['cleanSrcLine'] = $cleanSrcLine;
            $badLines[$srcLineNumber]['data'] = $dataLine;
//          throw new Exception("$errorMsg");
        }
        else if($dataLineCount < ($maxCol) ) {
            $errorMsg = "\nERROR: mauvais décopuage de la ligne \n---> pas assez de colonnes : $dataLineCount vs $maxCol colonnes attendues\n";
            echo "-------------------\n$errorMsg\n$cleanSrcLine\n"; print_r($dataLine); echo "\n\n";
            $badLines[$srcLineNumber]['srcLine'] = trim($srcLine);
            $badLines[$srcLineNumber]['cleanSrcLine'] = $cleanSrcLine;
            $badLines[$srcLineNumber]['data'] = $dataLine;
//          throw new Exception("$errorMsg");
        }
        else {

            foreach ($dataLine as $colKey => $colData) {
                $cleanColData = trim($colData);
                $dataLine[$colKey] = $cleanColData;
                if(!isset($colInfo[$colKey])) {
                    $colInfo[$colKey] = mb_strlen("$cleanColData");
                }
                else if (mb_strlen("$cleanColData") >  $colInfo[$colKey]) {
                    $colInfo[$colKey] = mb_strlen("$cleanColData");
                }
                else {
                    // ne rien faire (inférieur ou égal au max déjà stocké
                }
            }
            $selectedLines[$srcLineNumber]['srcLine'] = trim($srcLine);
            $selectedLines[$srcLineNumber]['cleanSrcLine'] = $cleanSrcLine;
            $selectedLines[$srcLineNumber]['data'] = $dataLine;
//            print_r($selectedLines);
//            print_r($colInfo);
//            exit();
        }
    }
}
ksort($outputLines);
//    print_r($badLines); // ----> 1 ligne
//    print_r($selectedLines);
//    print_r($colInfo);
//    print_r($outputLines);
//    exit();

/////////////////////////////////////////////////////////////////////////////////
foreach ($badLines as $badLineNumber => $badLineData) {
    $outputLines[$badLineNumber] = "-- MANUAL PROCESSING REQUIRED -- " . $badLineData['srcLine'];
    unset($srcLines[$badLineNumber]);
}
ksort($outputLines);
//    print_r($badLines); // ----> 1 ligne
//    print_r($selectedLines);
//    print_r($colInfo);
//    print_r($outputLines);
//    exit();



/////////////////////////////////////////////////////////////////////////////////
foreach ($selectedLines as $selectedLineNumber => $selectedLineData) {
    $selectedLineCols = $selectedLineData['data'];
    $outputLines[$selectedLineNumber] = "(" ;
    $selectedLineColsCount = count($selectedLineCols);
    foreach ($selectedLineCols as $selectedLineColKey => $selectedLineColData) {
        $maxSpaces = $colInfo[$selectedLineColKey];
        $colSql = "$selectedLineColData,";
        if ($selectedLineColsCount === ($selectedLineColKey + 1)) {
            $colSql = "$selectedLineColData";
        }
        $outputLines[$selectedLineNumber] .= str_pad_unicode("$colSql", $maxSpaces+2);
    }
    $outputLines[$selectedLineNumber] .= ")," ;
    unset($srcLines[$selectedLineNumber]);
}
ksort($outputLines);
//    print_r($badLines); // ----> 1 ligne
//    print_r($selectedLines);
//    print_r($colInfo);
//    print_r($outputLines);
//    exit();


$srcLinesCountFinal = count($srcLines);
$outputLinesCount = count($outputLines);
echo "\nFichiers de sortie   : $outputLinesCount lignes ";
echo "\nFichier d'entrée     : $srcLinesCount lignes au total";
echo "\nFichier d'entrée     : $srcLinesCountFinal lignes à traiter";
echo "\nLignes traités       : ". (int) ($genericSqlLineCount + $commentLineCount + count($selectedLines) + count($badLines)) . " (vérification)";
echo "\nLignes SQL générique : ". $genericSqlLineCount ." lignes";
echo "\nLignes commentaires  : ". $commentLineCount ." lignes";
echo "\nLignes preselection  : ". count($selectedLines) ." lignes";
echo "\nLinges en erreur     : ". count($badLines) ." lignes \n";

// create output file
$outputRaw = '';
foreach ($outputLines as $outputLineNumber => $outputLine) {
    $outputRaw .= "$outputLine\n";
}
file_put_contents("$outputPath", "$outputRaw");

/**
 * A proper unicode string padder
 * source: https://www.php.net/manual/en/function.str-pad.php#111147
 *
 * @param $str
 * @param $pad_len
 * @param $pad_str
 * @param $dir
 * @return mixed|string|null
 */
function str_pad_unicode($str, $pad_len, $pad_str = ' ', $dir = STR_PAD_RIGHT) {
    $str_len = mb_strlen($str);
    $pad_str_len = mb_strlen($pad_str);
    if (!$str_len && ($dir == STR_PAD_RIGHT || $dir == STR_PAD_LEFT)) {
        $str_len = 1; // @debug
    }
    if (!$pad_len || !$pad_str_len || $pad_len <= $str_len) {
        return $str;
    }

    $result = null;
    $repeat = ceil($str_len - $pad_str_len + $pad_len);
    if ($dir == STR_PAD_RIGHT) {
        $result = $str . str_repeat($pad_str, $repeat);
        $result = mb_substr($result, 0, $pad_len);
    } else if ($dir == STR_PAD_LEFT) {
        $result = str_repeat($pad_str, $repeat) . $str;
        $result = mb_substr($result, -$pad_len);
    } else if ($dir == STR_PAD_BOTH) {
        $length = ($pad_len - $str_len) / 2;
        $repeat = ceil($length / $pad_str_len);
        $result = mb_substr(str_repeat($pad_str, $repeat), 0, floor($length))
            . $str
            . mb_substr(str_repeat($pad_str, $repeat), 0, ceil($length));
    }
    return $result;
}
